const mongoose = require('mongoose');

const DB_URL = process.env.DB_URL;

// const DB_URL = 'mongodb://localhost:27017/node_exercise_3';


const connect = async () => {
    try {
        await mongoose.connect(DB_URL, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
        });
        console.log('Conectado a la DB')
    } catch(error) {
        console.log('Error conectando con la base de datos', error)
    }
}

module.exports = {connect: connect, DB_URL};

