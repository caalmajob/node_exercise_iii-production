const express = require('express');
const JobOffer = require('../models/JobOffer');

const router = express.Router();

router.get('/', (req, res) => {
    return res.render('index', { title: 'Mi servior', user: req.user});
});


module.exports = router;
